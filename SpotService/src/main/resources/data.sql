insert into parking_spot(id,wingnumber,blocknumber,location) values(1,1,1,'IN');
insert into parking_spot(id,wingnumber,blocknumber,location) values(2,1,2,'IN');
insert into parking_spot(id,wingnumber,blocknumber,location) values(3,1,3,'IN');
insert into parking_spot(id,wingnumber,blocknumber,location) values(4,2,1,'IN');

insert into parking_spot(id,wingnumber,blocknumber,location) values(5,2,2,'OUT');
insert into parking_spot(id,wingnumber,blocknumber,location) values(6,2,3,'OUT');
insert into parking_spot(id,wingnumber,blocknumber,location) values(7,1,1,'OUT');
insert into parking_spot(id,wingnumber,blocknumber,location) values(8,1,2,'OUT');
insert into parking_spot(id,wingnumber,blocknumber,location) values(9,2,1,'OUT');
insert into parking_spot(id,wingnumber,blocknumber,location) values(10,2,2,'OUT');


insert into employee_spot(id,spotid,empid) values(1,1,3);
insert into employee_spot(id,spotid,empid) values(2,2,4);
insert into employee_spot(id,spotid,empid) values(3,3,6);
insert into employee_spot(id,spotid,empid) values(4,4,10);
