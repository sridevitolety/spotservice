package com.org.spot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.spot.model.EmployeeSpot;

@Repository
public interface EmployeeSpotRepository extends JpaRepository<EmployeeSpot , Long> {
	
	@Query(value = "select * from employee_spot where empid = ?1", nativeQuery = true)
	EmployeeSpot finByEmpId(long empid);

}

