package com.org.spot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.spot.model.EmployeeSpot;
import com.org.spot.repository.EmployeeSpotRepository;

@Service
public class SpotService {
	
	@Autowired
	EmployeeSpotRepository sRepo;

	public long findByEmpid(long empid) {
		// TODO Auto-generated method stub
		EmployeeSpot es = sRepo.finByEmpId(empid);
		return es.getspotid();
	}

}
