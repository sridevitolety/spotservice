package com.org.spot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ParkingSpot {
	
	@Id
	@GeneratedValue
	private long id;
	private int wingnumber;
	private int blocknumber;
	private String location;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getWingnumber() {
		return wingnumber;
	}
	public void setWingnumber(int wingnumber) {
		this.wingnumber = wingnumber;
	}
	public int getBlocknumber() {
		return blocknumber;
	}
	public void setBlocknumber(int blocknumber) {
		this.blocknumber = blocknumber;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public ParkingSpot(long id, int wingnumber, int blocknumber, String location) {
		super();
		this.id = id;
		this.wingnumber = wingnumber;
		this.blocknumber = blocknumber;
		this.location = location;
	} 
	public ParkingSpot() {}
	@Override
	public String toString() {
		return "ParkingSpot [id=" + id + ", wingnumber=" + wingnumber + ", blocknumber=" + blocknumber + ", location="
				+ location + "]";
	}
	

}

