package com.org.spot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeeSpot {
	@Id
	@GeneratedValue
	private long id;
	private long spotid;
	private long empid;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getspotid() {
		return spotid;
	}
	public void setspotid(long spotid) {
		this.spotid = spotid;
	}
	public long getEmpid() {
		return empid;
	}
	public void setEmpid(long empid) {
		this.empid = empid;
	}
	public EmployeeSpot(long id, long spotid, long empid) {
		super();
		this.id = id;
		this.spotid = spotid;
		this.empid = empid;
	}	
	public EmployeeSpot() {}
	@Override
	public String toString() {
		return "EmployeeSpot [id=" + id + ", spotid=" + spotid + ", empid=" + empid + "]";
	}
}

