package com.org.spot.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.spot.service.SpotService;

@RestController
@RequestMapping("/spot")
public class SpotController {
	@Autowired
	SpotService sServ;
	
	@GetMapping("/{empid}")
	public long getSpotId(@PathVariable long empid) {
		return sServ.findByEmpid(empid);
	}

}

