package com.org.spot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpotServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotServiceApplication.class, args);
	}

}
